//this function can remove a array element.
    Array.remove = function(array, from, to) {
        var rest = array.slice((to || from) + 1 || array.length);
        array.length = from < 0 ? array.length + from : from;
        return array.push.apply(array, rest);
    };

    // Build the UI for a new message and add to the DOM
    function addMessageChannel(data) {alert('addMessageChannel');  
        // Create element from template and set values
        var chat_element = createMessageElement(); 
        chat_element.find('#message-sender-chat').html(data.text); 
        chat_element.find('#message-sender_name').text(data.sender_name);
        chat_element.find('#message-receiver-name').text(data.receiver_id);
        chat_element.find('#message-sender-timestamp').text(data.created_at);
        //chat_element.find('#message-receiver-avatar').attr('src', data.avatar)        
        
        //Search in active windows for assign chat message
        console.log(chat_element);
        $("div.content").find("#container-"+data.receiver_id+"-messages").css({"color": "blue", "border": "2px solid red"});
        var receiver_messages = $("div.content").find("#container-"+data.receiver_id+"-messages");
        receiver_messages.append(chat_element)
        
        console.log(chat_element);
        $("div.content").find("#container-"+data.sender_id+"-messages").css({"color": "red", "border": "2px solid red"});
        var sender_messages = $("div.content").find("#container-"+data.sender_id+"-messages");
        sender_messages.append(chat_element)

        // Make sure the incoming message is shown
        receiver_messages.scrollTop(receiver_messages[0].scrollHeight);
        sender_messages.scrollTop(sender_messages[0].scrollHeight);
    }

     // Creates an activity element from the template
    function createMessageElement() {
        var text = $('#chat_message_template').text();
        var el = $(text);
        return el;
    }

    //this variable represents the total number of popups can be displayed according to the viewport width
    var total_popups = 0;
   
    //arrays of popups ids
    var popups = [];

    //this is used to close a popup
    function close_popup(id)
    {   console.log(popups);
        for(var iii = 0; iii < popups.length; iii++)
        {
            if(id == popups[iii])
            {
                Array.remove(popups, iii);
                $('#'+id).css('display','none');
               
                calculate_popups();
               
                return;
            }
        }  
    }

    //displays the popups. Displays based on the maximum number of popups that can be displayed on the current viewport width
    function display_popups()
    {
        var right = 220; 
       
        var iii = 0;
        for(iii; iii < total_popups; iii++)
        {
            if(popups[iii] != undefined)
            {   //console.log(popups);
                var element = $('#'+popups[iii]);
                element.css('right',right + 'px');
                right = right + 320;
                element.css('display','block');
            }
        }
       
        for(var jjj = iii; jjj < popups.length; jjj++)
        {
            var element = $('#'+popups[jjj]);
            element.css('display','none');
        }
    }
   
    //creates markup for a new popup. Adds the id to popups array.
    function register_popup(id,sender_id,receiver_id,name)
    {
       
        for(var iii = 0; iii < popups.length; iii++)
        {  
            //already registered. Bring it to front.
            if(id == popups[iii])
            {
                Array.remove(popups, iii);
           
                popups.unshift(id);
               
                calculate_popups();
               
               
                return;
            }
        }
        var new_id = receiver_id + '_'+id;         
        var btn_close="javascript:close_popup(\'"+ new_id +"\');" ;
        var element = $("#liveChat").clone(); 
        //console.log(element);
        element.prop('id', new_id );
        element.attr('data-receiver-user_id', receiver_id );
        element.attr('data-sender-user_id', sender_id );
        element.find('div.popup-messages').prop( 'id','container-'+ receiver_id  +'-messages' );
        element.find('div.direct-chat-messages').attr( 'data-sender-user_id', sender_id );
        element.find('div.direct-chat-messages').attr( 'data-receiver-user_id', receiver_id );  

        element.find('#txt-chat-input-message').attr('data-receiver-user_id',receiver_id);
        element.find('#txt-chat-input-message').attr('data-sender-user_id',sender_id);
        element.find('#txt-chat-input-message').attr('data-conversation_id',id);
        element.find('div.popup-head-left').text( name );
        element.find('div.popup-head-right a.btn_close').attr( 'href', btn_close );
        $(element).appendTo('div.content');

        popups.unshift(new_id);
               
        calculate_popups();
        
        new_chat_popup(new_id,sender_id,receiver_id,name)

    }
   
    //calculate the total number of popups suitable and then populate the toatal_popups variable.
    function calculate_popups()
    {
        var width = window.innerWidth;
        if(width < 540)
        {
            total_popups = 0;
        }
        else
        {
            width = width - 200;
            //320 is width of a single popup box
            total_popups = parseInt(width/320);
        }
       
        display_popups();
       
    }

    //recalculate when window is loaded and also when window is resized.
    window.addEventListener("resize", calculate_popups);
    window.addEventListener("load", calculate_popups);

    //Create new chat popup and your private channel to talk
   function new_chat_popup(id,sender_id,receiver_id,name) {

        var pusher = new Pusher('c02824b527f184b4ac94', {
            encrypted: true,
            //authEndpoint: "/laravelchat/public/chat/auth",
            auth: {
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            }
        });

        // Trigger event on both user channels with one call
        var channels = [ "conversation-private_"+sender_id, 
                         "conversation-private_"+receiver_id
                      ];

        var chatChannel = pusher.subscribe( channels );
        
        var eventData = {
                          'channel_name': "private-chat-"+sender_id+'-'+receiver_id,
                          'initiated_by': sender_id,
                          'chat_with'   : receiver_id
                        };

        var info_push = {
                'channels': chatChannel,
                'eventData': eventData, 
            };
        URIpath= window.location.pathname;
        URIpath +='/get-user-conversation'

        params = {id: receiver_id, conversation_id: id};

        $.ajax({
                url: URIpath,
                type: "POST",
                data: params,
                dataType: "json",
                success : function(data){

                $.each(data,function(value){
                    
                    $.each(this, function(index, value) { 
                            var template = createMessageElement();
                            //console.log(value);
                            template.find('#message-sender-chat').html(value.message); 
                            template.find('#message-sender-name').text(value.sender_name);
                            //template.find('#message-sender-avatar').attr('src', value.avatar)
                            
                            // Utility to build nicely formatted time
                            template.find('#message-sender-timestamp').text(value.created_at);
                            
                            template.find('#message-receiver-name').text(value.receiver_name);
                            var messages = $("div.content").find("#container-"+receiver_id+"-messages");
                                messages.append(template)
                            // Make sure the incoming message is shown
                            messages.scrollTop(messages[0].scrollHeight); 
                         
                        });
                    });
                }
        });
  
   }



