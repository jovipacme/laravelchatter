    function initChat() {
        // send button click handling
        $('.chat_btn-message').click(sendMessage( $(this) ) );
        //$('.chat_input-message').keypress(checkSend);
    }

    // Send on enter/return key
    /*function checkSend(e) {
        if (e.keyCode === 13) {
            return sendMessage();
        }
    }*/

    // Handle the send button being clicked
    function sendMessage(input_text) {
        var messageText = input_text.val();
        if(messageText.length < 3) {
            return false;
        }

        // Build POST data and make AJAX request
        var data = {chat_text: messageText};
        $.post("{{ route('chat.private-chat')}}", data).success(sendMessageSuccess(input_text)  );

        // Ensure the normal browser event doesn't take place
        return false;
    }

    // Handle the success callback
    function sendMessageSuccess(input_text) {
        input_text.val('')
        console.log('message sent successfully');
    }

    // Build the UI for a new message and add to the DOM
    function addMessage(data) {
        // Create element from template and set values
        var el = createMessageEl(); 
        el.find('#message-sender-chat').html(data.text); 
        el.find('#message-sender_name').text(data.sender_name);
        el.find('#message-receiver-name').text(data.receiver_id);
        // Utility to build nicely formatted time
        el.find('#message-sender-timestamp').text(data.created_at);
        //el.find('#message-receiver-avatar').attr('src', data.avatar)        
        
        var messages = $('.direct-chat-messages');
        messages.append(el)
        
        // Make sure the incoming message is shown
        messages.scrollTop(messages[0].scrollHeight);
    }

    // Creates an activity element from the template
    function createMessageEl() {
        var text = $('#chat_message_template').text();
        var el = $(text);
        return el;
    }

