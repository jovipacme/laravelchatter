@extends('layouts.chat')

@section('content')
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    <a href="{{ url('/login') }}">Login</a>
                    <a href="{{ url('/register') }}">Register</a>
                </div>
            @endif

            <div class="content">
                <div class="row"> 
                @if(Session::has('user'))
                    <div class="alert-box success">
                        <h2>{{ Session::get('user') }}</h2>
                    </div>
                @endif
                </div>
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-3"></div>
                    <div class="col-md-3" id='widgetChat'>
                        @include('messenger.widget')
                    </div>
                    <div class="col-md-3" id='sidebarRight'>
                        @include('messenger.members-chat', array('users' => $users))                 
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('scripts')
<link href="{{ asset('css/chat-multiple.css') }}" rel="stylesheet">
<script src="https://cdn.rawgit.com/samsonjs/strftime/master/strftime-min.js"></script>
<script id="multiple-chat-script" src="{{ asset('js/chat-multiple.js') }}"></script>

<script>


    // Ensure CSRF token is sent with AJAX requests
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // Added Pusher logging
    Pusher.log = function(msg) {
        console.log(msg);
    };

    // Handle the send button being clicked
    function sendMessage(input_text) {
        var messageText = input_text.val();
        var receiver_user_id = input_text.attr('data-receiver-user_id');
        var sender_user_id = input_text.attr('data-sender-user_id');
        var conversation_id = input_text.attr('data-conversation_id');
        var datetime = strftime( '%Y-%m-%d %H:%M:%S', new Date() );

        if(messageText.length < 3) {
            return false;
        }

        // Build POST data and make AJAX request
        var params = {
                chat_text: messageText,
                conversation_id: conversation_id, 
                sender_id: sender_user_id,
                receiver_id: receiver_user_id,
                timestamp: datetime,
            };

        $.ajax({
            url: "{{ route('chat.private-chat') }}",
            type: "POST",            
            data: params,
            dataType: "json",
            success : function(data){
            
                input_text.val('');

                $.each(data,function(index, value){
                    var template = createMessageEl();
                        var timestamp = strftime( '%H:%M:%S %P', new Date() );
                        console.log(value);
                        template.find('#message-sender-chat').html(value.message); 
                        template.find('#message-sender-name').text(value.sender_name);
                        //template.find('#message-sender-avatar').attr('src', value.avatar)
                        template.find('#message-sender-timestamp').text(timestamp);
                        template.find('#message-sender_name').text(value.sender_name);
                        template.find('#message-receiver-name').text(value.receiver_name);
                        //$("div.content").find("#container-"+receiver_user_id+"-messages").css({"border": "2px solid #a94442"});
                    var receiver_messages = $("div.content").find("#container-"+receiver_user_id+"-messages");
                        receiver_messages.append(template);

                        // Make sure the incoming message is shown
                        receiver_messages.scrollTop(receiver_messages[0].scrollHeight);
                    });
                console.log('message sent successfully');    
                }            
        });

        // Ensure the normal browser event doesn't take place
        return false;
    }

    // Handle the success callback
    function sendMessageSuccess(input_text) {
        input_text.val('')
        console.log('message sent successfully');
    }

    // Build the UI for a new message and add to the DOM
    function addMessage(data) {
        // Create element from template and set values
        var chat_element = createMessageEl(); 
        chat_element.find('#message-sender-chat').html(data.message); 
        chat_element.find('#message-sender_name').text(data.sender_name);
        chat_element.find('#message-receiver-name').text(data.receiver_name);
        chat_element.find('#message-sender-timestamp').text(data.timestamp);
        //chat_element.find('#message-receiver-avatar').attr('src', data.avatar)        
        
        //Search in active windows for assign chat message       
        var sender_messages = $("div.content").find("#container-"+data.sender_id+"-messages");
        sender_messages.append(chat_element)

        // Make sure the incoming message is shown
        sender_messages.scrollTop(sender_messages[0].scrollHeight);
    }

    // Creates an activity element from the template
    function createMessageEl() {
        var text = $('#chat_message_template').text();
        var el = $(text);
        return el;
    }
 
$(document).ready(function(){

    $("#addClass").click(function () {
        $('#liveChat').addClass('popup-box-on');
        
        $("#sideChatRight").toggleClass("collapsed");
        
    });
          
    $("#removeClass").click(function () {
        $('#liveChat').removeClass('popup-box-on');
    });

    // send button click handling
    $("div.content").on( "click",".chat_btn-message",function () {
        var text_input = $(this).parent().parent().find('.chat_input-message');
        //console.log( $(this).parent().parent().find('.chat_input-message').val() );
        sendMessage( text_input );
    });
    $("div.content").on( "keypress",".chat_input-message",function (e) {
        if (e.keyCode === 13) {
            return sendMessage($(this));
        }        
    });


    $(".toggle-sidechat").click(function () {
            $("#sideChatRight").toggleClass("collapsed");
            $("#content").toggleClass("col-md-12 col-md-9");
            
            return false;
    });


});

    var pusher = new Pusher("{{env("PUSHER_KEY")}}", {
        encrypted: true,
        authEndpoint: "{{ url('/chat/auth') }}",
        auth: {
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }
    });

//  join the presence-software channel
    var mainChannel = pusher.subscribe('presence-global');
       // mainChannel.bind('pusher:subscription_succeeded', addMessage);

       //chatChannel.bind('pusher:member_added', memberAdded);
       //chatChannel.bind('pusher:member_removed', memberRemoved);

    var privateChannel = pusher.subscribe('conversation-private_{{ Auth::user()->id }}');
    privateChannel.bind('private-new-message', addMessage);


</script>
@endsection