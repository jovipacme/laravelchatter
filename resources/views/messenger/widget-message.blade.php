
<!-- Message. Datetime out -->
<div class="chat-box-single-line">
    <abbr class="timestamp">October 8th, 2015</abbr>
</div>

<!-- Message. Default to the left -->
<div class="direct-chat-msg doted-border">
  <div class="direct-chat-info clearfix">
    <span class="direct-chat-name pull-left">{{ $message->receiver_id }}</span>
  </div>
  <!-- /.direct-chat-info -->
  <img alt="message user image" src="http://bootsnipp.com/img/avatars/bcf1c0d13e5500875fdd5a7e8ad9752ee16e7462.jpg" class="direct-chat-img"><!-- /.direct-chat-img -->
  <div class="direct-chat-text">
    {{ $message->chat}}
  </div>
  <div class="direct-chat-info clearfix">
    <span class="direct-chat-timestamp pull-right">{{ $message->created_at->toTimeString() }}</span>
  </div>
    <div class="direct-chat-info clearfix">
    <span class="direct-chat-img-reply-small pull-left">  </span>
    <span class="direct-chat-reply-name">{{ $message->sender_id }}</span>
    </div>
  <!-- /.direct-chat-text -->
</div>
<!-- /.direct-chat-msg -->