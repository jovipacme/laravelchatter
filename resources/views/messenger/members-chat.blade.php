<link href="{{ asset('css/sidebar-chat.css') }}" rel="stylesheet">

<style>
#row-main {
  /* necessary to hide collapsed sidebar */
  overflow-x: hidden;
}

#sideChatRight {
  /* for the animation */
  max-height: 250px;
  transition: margin 0.3s ease;
}

.collapsed {
  /* hide it for small displays*/
  display: none;
}

</style>
<!--/.sidebar Chat -->
    <div id="sideChatRight" class="panel panel-default">
      <div class="panel-heading">
          <button type="button" class="btn btn-default toggle-sidechat">Toggle</button>
      </div>
      <div class="panel-body">
        <div id="members_list" class="member_list">
           <ul class="list-unstyled">
              @foreach ($users as $user)           
              <li class="left clearfix">
                 <span class="chat-img pull-left">
                    <span class="pending-chat-link" id="pending-chat-link_{{$user->id}}" ></span>
                    <img src="" alt="User Avatar" class="img-circle">
                 </span>
                 <div class="chat-body clearfix">                    
                    <a class="start-chat-link" id="start-chat-link_{{$user->id}}"
                    href="javascript:register_popup('{{ $user->id . csrf_token() }}','{{ Auth::id() }}','{{ $user->id }}','{{ $user->name }}' );">
                    <div class="header_sec">
                       <strong class="primary-font">{{ $user->name }}</strong> 
                       <span class="label label-success pull-right">&nbsp;</span>
                    </div>
                    <div class="contact_sec">
                       <i> Messages</i> <span id="count_conversations_{{$user->id}}" class="badge pull-right count_conversations"></span>
                    </div>
                    </a>
                 </div>
              </li>
              @endforeach
           </ul>
        </div>
      </div>
    </div>
<!--/.well -->

    <script>
        // Build the chat link for a new conversation
        var getChatLinkHtml = function(conversationId, memberId) {
            return '<a href="{{ url('/chat/') }}/' + conversationId + '/' +  memberId + '"><span class="glyphicon glyphicon-comment"></span></a>';
        };
        var getChatLinkPopup = function(conversationId,memberId,memberName) {
            return 'javascript:register_popup("'+conversationId+'","' + '{{ Auth::id() }}' +'","' +memberId+ '","'+memberName+'")';
        };

        var pusher = new Pusher("{{env("PUSHER_KEY")}}", {
            encrypted: true,
            authEndpoint: "{{ url('/chat/auth') }}",
            auth: {
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            }
        });
        Pusher.channel_auth_endpoint = "{{ url('/chat/auth') }}";

        var presenceChannel = pusher.subscribe('presence-global');
        var count = presenceChannel.members.count;
        presenceChannel.members.each(function(member) {
          var userId = member.id;
          var userInfo = member.info;
        });



        $(function() {
            $.ajax({
                url: "{{ url('/api/v1/users?api_token=') }}" + window.Laravel.apiToken
            }).done(function(data) {
                var conversationId = new Date().getTime();
                data = $.parseJSON(data);
                $.each(data, function(index, member) {
                    $('#pending-chat-link_'+member.id).html( getChatLinkHtml(conversationId, member.id) );
                    $('#start-chat-link'+member.id).attr('conversationId',conversationId );
                   $('#start-chat-link_'+member.id).attr('href',getChatLinkPopup(conversationId, member.id,member.name));                                      
                });
            });
        });
    </script>