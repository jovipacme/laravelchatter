<div class="popup-box chat-popup" id="liveChat" data-sender-user_id="{{ Auth::user()->id }}" data-receiver-user_id="">
              <div class="popup-head">
                <div class="popup-head-left pull-left"><span class="glyphicon glyphicon-comment"></span> Gurdeep Osahan</div>
                    <div class="popup-head-right pull-right">
                        <a href="#" data-widget="remove" class="btn_close">
                          <span class="glyphicon glyphicon-remove icon_close" data-id="chat_window"></span>
                        </a>
                    </div>
              </div>
            <div id="container-{{ Auth::user()->id }}-messages" class="popup-messages">
                <div class="direct-chat-messages" data-sender-user_id="{{ Auth::user()->id }}" data-receiver-user_id="">
                </div>
            </div>
            <div class="popup-messages-footer">
                <div class="input-group">
                    <input id="txt-chat-input-message" class="form-control input-sm chat_input-message" placeholder="Type a message..." type="text">
                    <span class="input-group-btn">
                    <button class="btn btn-primary btn-sm chat_btn-message" id="btn-chat-send-message">Send</button>
                    </span>
                </div>            
                <div class="btn-footer"> 
                </div>
            </div>
</div>
<script id="chat_message_template" type="text/template">
<!-- Message. Default to the left -->
<div class="direct-chat-msg doted-border">
  <div class="direct-chat-info clearfix">
    <span id="message-sender_name" class="direct-chat-name pull-left"></span>
  </div>
  <!-- /.direct-chat-info -->
  <img id="message-sender-avatar" alt="avatar" src="" class="direct-chat-img"><!-- /.direct-chat-img -->
  <div id="message-sender-chat" class="direct-chat-text"> </div>
  <div v-for="item in messages" class="direct-chat-info clearfix">
    <span id="message-sender-timestamp" class="direct-chat-timestamp pull-right"></span>
  </div>
    <div class="direct-chat-info clearfix">
    <span id="message-receiver-state" class="direct-chat-img-reply-small pull-left">  </span>
    <span id="message-receiver-name" class="direct-chat-reply-name"></span>
    </div>
  <!-- /.direct-chat-text -->
</div>
<!-- /.direct-chat-msg -->
</script>