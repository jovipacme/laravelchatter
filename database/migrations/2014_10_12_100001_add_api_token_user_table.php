<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddApiTokenUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * ALTER TABLE `dev_users` ADD `api_token` VARCHAR(16) NULL AFTER `password`;
	 * @return void
     */
    public function up()
    {
		Schema::table('users', function (Blueprint $table) {
			$table->char('api_token', 60)->nullable()->after('remember_token');
		});

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::drop('users');
    }
}