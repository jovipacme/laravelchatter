<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class Agencies extends Model
{
    
	public $table = "agencies";
    

	public $fillable = [
	    "name",
		"email",
		"photo",
		"address1",
		"adress2"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "name" => "string",
		"photo" => "string",
		"address1" => "string",
		"adress2" => "string"
    ];

	public static $rules = [
	    "name" => "required"
	];

}
