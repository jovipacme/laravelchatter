<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatMessage extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'conversation_id', 'sender_id', 'receiver_id', 'message', 'sender_name', 'receiver_name','read',
    ];
    // protected $attributes = ['sender', 'receiver'];
    protected $appends = ['sender', 'receiver'];

    /**
     * The user that initiating the conversation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sender()
    {
        return $this->belongsTo(User::class, 'user_id', 'sender_id');
    }

    /**
     * The user receiving the message.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function receiver()
    {
        return $this->belongsTo(User::class, 'user_id', 'receiver_id');
    }

    public function getSenderAttribute()
    {
        return User::where('id', $this->sender_id)->first();
    }
    public function getReceiverAttribute()
    {
        return User::where('id', $this->receiver_id)->first();
    }

}
