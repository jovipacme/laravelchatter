<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::auth();

Route::get('/home', 'HomeController@index');

Route::get('/', function () {
    return view('welcome');
});


/*Rutas privadas solo para usuarios autenticados*/
Route::group(['before' => 'auth'], function()
{  
	Route::post('/user/list', 'UserController@list')->name('user.list');  

/*Chat urls for implement*/
    Route::get('/listen', 'MessengerController@listen');
//  Route::get('/start-conversation', 'MessengerController@start');
    Route::get('/chat/{conversationId}/{representativeId}', 'MessengerController@chat');
    Route::get('/conversations', 'MessengerController@conversations');
    Route::get('/conversation/{conversationId}', 'MessengerController@conversation');

//gobal chat    
    Route::get('/chat', 'ChatController@getIndex')->name('chat.index');
    Route::post('/chat/message', 'ChatController@postMessage')->name('chat.message');

 /*Chat urls*/
    Route::get('/start-conversation', 'HomeController@getChatroom')->name('start-conversation');

    Route::post('/start-conversation/get-user-conversation', 'ChatController@getUserConversationById')->name('user-id-conversation');

    Route::post('/chat/save-chat', 'ChatController@saveUserChat')->name('chat.private-chat');

    Route::post('/chat/auth', 'ChatController@postAuth')->name('chat.auth');

    Route::get('/chat/get-user-messages', 'ChatController@getUserConversation')->name('user-messages');

/* Notification url for implement*/
	Route::get('/notifications', 'NotificationController@getIndex')->name('notifications.index');

	Route::post('/notifications/notify', 'ChatController@postNotify')->name('notifications.notify');

});