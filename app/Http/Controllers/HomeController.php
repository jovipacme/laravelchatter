<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\User; 
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    const DEFAULT_CHAT_CHANNEL = 'chat-private';

    public function __construct()
    {
        $this->middleware('auth'); 
        $this->pusher = App::make('pusher');
        if(!Auth::guest()) {
            $this->user_id = Auth::user()->id;
            $this->user = Auth::user()->name;
            $this->avatar =  Auth::user()->name;
        }
        $this->chatChannel = self::DEFAULT_CHAT_CHANNEL;          
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    
    public function getChatroom()
    {   
        $chatChannel = $this->chatChannel;
        //Users and except the current user
        
        $users = User::orderBy('name', 'asc')
            ->where('id','<>',$this->user_id)
            //->whereNotNull('deleted_at')
            ->get();

        return view('start',compact('chatChannel', 'users') );
    }    
}
