<?php

namespace App\Http\Controllers;
use App\ChatMessage;
use App\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
//use Illuminate\Support\Facades\Response;

use Vinkla\Pusher\Facades\Pusher as LaravelPusher;

class ChatController extends Controller
{
    var $pusher;
    var $user;
    var $chatChannel;

	const DEFAULT_PAGE_SIZE = '5';
    const DEFAULT_CHAT_CHANNEL = 'chat-global';

    public function __construct()
    {
        $this->pusher = App::make('pusher');
        if(!Auth::guest()) {
	        $this->user = Auth::user()->name;
	        $this->avatar =  Auth::user()->name;
        }
        $this->chatChannel = self::DEFAULT_CHAT_CHANNEL;
    }

    public function getIndex()
    {
        if(Auth::guest())
        {
        	return redirect('login');
        }

        return view('messenger.chat', ['chatChannel' => $this->chatChannel]);
    }

    public function start_chat(Request $request)
    {   $this->chatChannel = $request->input('channels');
        $message = $request->input('eventData');

        $this->pusher->trigger($this->chatChannel, 'one-to-one-chat-request', $message);
    }


    public function postMessage(Request $request)
    {   //Simple example for send message
        $message = [
            'text' => e($request->input('chat_text')),
            'username' => $this->user,
            'avatar' => $this->avatar,
            'timestamp' => (time()*1000)
        ];
        $this->pusher->trigger($this->chatChannel, 'new-message', $message);
    }

    //Implements authentication for specific channel
	public function postAuth(Request $request)
	{	
        if(!Auth::guest())
        {
          $socketId = $request->input('socket_id');	
		  $channelName = $request->input('channel_name');
          $user_id = Auth::user()->id;
          $presence_data = Auth::user();

		  $auth = $this->pusher->socket_auth($channelName, $socketId,$user_id,$presence_data);
			//return response(['auth' => $auth], 200);
			return $auth;
		} else {
          //header('', true, 403);		  
		  return response(['auth' => 'Unauthorized'], 200);
		}
		
	}

    public function postAuthPresence(Request $request)
    {   
        if(!Auth::guest())
        {
          $socketId = $request->input('socket_id'); 
          $channelName = $request->input('channel_name');
          $user_id = Auth::user()->id;
          $presence_data = Auth::user();

          $auth = $this->pusher->presence_auth($channelName, $socketId,$user_id,$presence_data);
            //return response(['auth' => $auth], 200);
            return $auth;
        } else {
          return 'Unauthorized';
        }
        
    }

   /**
     * Display all messages in an existing conversation
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Make sure the logged-in user participates in the conversation
        $user = Auth::user();

        return DB::table('chat_messages')
            ->where('conversation_id', '=', $id)
            ->where(function ($query) use ($user) {
                $query
                    ->where('sender_id', '=', $user->id)
                    ->orWhere('receiver_id', '=', $user->id);
            })
            ->take(self::DEFAULT_PAGE_SIZE)
            ->orderBy('created_at', 'desc')
            ->get();
            //->toJson();
    }

    /**
     * Remove a conversation
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Make sure the logged-in participates the conversation
        $user = Auth::guard('api')->user();
        DB::table('chat_messages')
            ->where('conversation_id', '=', $id)
            ->where(function ($query) use ($user) {
                $query
                    ->where('sender_id', '=', $user->id)
                    ->orWhere('receiver_id', '=', $user->id);
            })
            ->delete();

        // Return a no content response
        return Response::json([], 204);
    }

    /**
     * Display a list of conversations for the current user
     *
     * @return \Illuminate\Http\Response
     */
    public function getUserConversation()
    {
        // Make sure the logged-in user participates in the conversations
        $user = Auth::user();

        return DB::table('chat_messages')
            ->select(DB::raw('conversation_id, COUNT(message) AS messages, MAX(created_at) AS date'))
            ->where(function ($query) use ($user) {
                $query
                    ->where('sender_id', '=', $user->id)
                    ->orWhere('receiver_id', '=', $user->id);
            })
            ->groupBy('conversation_id')
            ->orderBy('date', 'desc')
            ->take(self::DEFAULT_PAGE_SIZE)
            ->get();
            //->toJson();
    }

    public function getUserConversationById (Request $request)
    {
        $userId = $request->input('id');
        $authUserId = $request->user()->id;
        $chats = ChatMessage::whereIn('sender_id', [$authUserId,$userId])
            ->whereIn('receiver_id', [$authUserId,$userId])
            ->orderBy('created_at', 'asc')
            ->get();
        return response(['data' => $chats], 200);
    }

    public function saveUserChat (Request $request)
    {
        $sender_id = $request->input('sender_id');
        $receiver_id = $request->input('receiver_id');

        $sender_info = User::where('id',$sender_id)->first();
        $receiver_info = User::where('id',$receiver_id)->first();

        $sender_name = $sender_info->name;
        $receiver_name = $receiver_info->name;        
        $conversation_id = $request->input('conversation_id');
        $chatText = e($request->input('chat_text'));

        $message = [
            'sender_id' => $sender_id,
            'sender_name' => $sender_name,            
            'receiver_id' => $receiver_id,         
            'receiver_name' => $receiver_name,
            'message' => $chatText,
            'avatar' => $this->avatar,
            'conversation_id'=>$conversation_id, 
            'timestamp' => date('Y-m-d H:i:s')
            //'timestamp' => (time()*1000)
        ];


        $data = [
            'sender_id' => $sender_id, 
            'sender_name' => $sender_name,
            'receiver_id' => $receiver_id,
            'receiver_name' => $receiver_name,
            'conversation_id'=>$conversation_id, 
            'message' => $chatText, 
            'read' => 1
        ];
        $chat = ChatMessage::create($message);
        $saved_message = ChatMessage::where('id', $chat->id)->first();
        
        $this->pusher->trigger('conversation-private_'.$receiver_id, 'private-new-message', $message);
        //return view('messenger.widget-message',compact('message') );
        return response(['data' => $data], 201);
    }

}