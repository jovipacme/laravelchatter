<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class UserController extends Controller
{
    const USER_TYPE_REPRESENTATIVE = 'representative';
    const USER_TYPE_CUSTOMER = 'agent';

    /**
     * Display a list of chat representatives
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Make sure the user is logged-in
        $user = Auth::guard('api')->user();

        return User::orderBy('name', 'asc')
            //->whereNotNull('deleted_at')
            ->get()
            ->toJson();
    }

    public function list()
    {
        // Make sure the user is logged-in
        $this->user_id=Auth::guard('api')->user()->id;
        if ($this->user_id) {
            $users = User::where('id','<>',$this->user_id)
                ->whereNotNull('deleted_at')
                ->orderBy('name', 'asc')
                ->get();
            return response(['data' => $users], 200);
        } else
            return Response::json([], 204);
    }


    /**
     * Change a representative's name
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Make sure the logged-in user is a representative
        $user = Auth::guard('api')->user();
        /*if ($user->type != self::USER_TYPE_REPRESENTATIVE) {
            return Response::json([], 403);
        }*/

        $user = User::find($id);
        $user->name = $request->name;
        $user->save();

        return $user->toJson();
    }
}
