# Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Laravel Chat by Pusher.js

Installation

Require this package, with Composer, in the root directory of your project.

$ composer require vinkla/pusher "2.3.*"
Add the service provider to config/app.php in the providers array.

Vinkla\Pusher\PusherServiceProvider::class

#Configuration
Laravel Pusher requires connection configuration. 

app/config/pusher.php -- For change main configuration to Pusher account

Add CRF token in main layout

<meta name="csrf-token" content="{{ csrf_token() }}" />


## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
